from flask import render_template, flash, request
from app import app
from forms import *

@app.route('/')
@app.route('/index')
def index():
	return render_template("index.html", title = "Digital library")

@app.route('/signup')
def signup():
	type = request.args.get('type_user')
	if(type == "pupil"):
		return render_template("form.html", title = "Регистрация ученика", content = "signup.html", form = LoginForm())
	elif(type == "teacher"):
		pass
	elif(type == "librarian"):
		pass
	return "Плохой запрос!"

@app.route('/pipil/<all>'):