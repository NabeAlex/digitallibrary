from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, SelectField
from wtforms.validators import Required, Length

class LoginForm(Form):
    id_pupil = TextField('id', validators = [Required(), Length(min = 7, max = 7)])

    name = TextField('name', validators = [Required(), Length(min = 1, max = 15)])
    last_name = TextField('last_name', validators = [Required(), Length(min = 1, max = 20)])


    classes = [("", "")]
    for i in range(1, 12):
    	classes.append((i, i))


    select_class = SelectField('class', default = 0, choices = classes)
    number_class = SelectField('number', default = 0, choices = [("", ""), ("a", "а"), ("b", "б"), ("c", "в"), \
    																				     ("d", "г"), ("e", "д")])

    email = TextField('email')
    phone = TextField('phone')

    remember_me = BooleanField('remember_me', default = False)

